import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  data = [];
  constructor(private http: HttpClient) { }

  upload(file: any, k: number, stype: number) {
    this.http.post<any[]>("http://localhost:8000/upload/" + stype, { bytes: [...(new Uint8Array(file))], k: k }).subscribe(
      data => {
        this.data = []
        for(let i of data) {
          let values = i.split('/');
          let text = "../../assets/" + values[values.length-3] + '/' + values[values.length-2] + '/' + values[values.length-1];
          this.data.push(text);
        }
      }
    );
  }
}
