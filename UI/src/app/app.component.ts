import { Component } from '@angular/core';
import { UploadService } from './upload/upload.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  file = null
  k = 0
  search_type = 0

  constructor(private upl: UploadService) {}

  fileChange(files: FileList) {
    let fileReader = new FileReader();
    fileReader.addEventListener('loadend', () => {
      this.file = fileReader.result;
      // console.log(this.file);
      // console.log("Array contains", this.file.byteLength, "bytes.");
    });
    fileReader.readAsArrayBuffer(files.item(0));
  }

  upload() {
    this.upl.upload(this.file, this.k, this.search_type);
  }
}
