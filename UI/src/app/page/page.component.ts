import { Component, OnInit } from '@angular/core';
import { UploadService } from '../upload/upload.service';
@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss']
})
export class PageComponent implements OnInit {
  constructor(public upd: UploadService) { }

  ngOnInit(): void {
  }
}
