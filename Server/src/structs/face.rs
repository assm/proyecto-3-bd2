extern crate image;

use std::path::*;
use face_recognition::*;
use serde::{Serialize, Deserialize};
use face_recognition::face_encoding::*;
use face_recognition::face_detection::*;
use crate::structs::point::*;
use face_recognition::landmark_prediction::*;

pub struct FaceParser {
  detector: FaceDetector,
  landmarks: LandmarkPredictor,
  model: FaceEncodingNetwork
}

impl FaceParser {
  pub fn new() -> Self {
    FaceParser {
      detector: FaceDetector::new(),
      landmarks: LandmarkPredictor::new(PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("files").join("shape_predictor_68_face_landmarks.dat")).unwrap(),
      model: FaceEncodingNetwork::new(PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("files").join("dlib_face_recognition_resnet_model_v1.dat")).unwrap()
    }
  }

  pub fn parse(&self, path: &Path) -> FaceImage {
    FaceImage::new(path, &self)
  }

  pub fn parsemem(&self,byteslice: &[u8]) -> Vec<f64> {
    FaceImage::newbyarr(byteslice, &self).vector_puntos
  }
}

#[derive(Serialize, Deserialize)]
pub struct FaceImage {
  pub path: String,
  pub dirpath: String,
  pub vector_puntos: Vec<f64>
}

impl FaceImage {
  pub fn new(imagepath: &Path, parser: &FaceParser) -> Self {
    let image = image::open(&imagepath).unwrap().to_rgb();
    let matrix = ImageMatrix::from_image(&image);
    let val1 = parser.detector.face_locations(&matrix);
    if val1.len() < 1 { return FaceImage{ path: "none".to_string(), vector_puntos: Vec::new(), dirpath: "".to_string() }; }
    let val2 = parser.landmarks.face_landmarks(&matrix, &val1[0]);
    let val3 = parser.model.get_face_encodings(&matrix, &[val2], 0);
    let points = &val3[0];
    FaceImage{
      path: imagepath.display().to_string(),
      dirpath: "".to_string(),
      vector_puntos: points.to_vec()
    }
  }

  pub fn newbyarr(byteslice: &[u8], parser: &FaceParser) -> Self {
    let image = image::load_from_memory(byteslice).unwrap().to_rgb();
    let matrix = ImageMatrix::from_image(&image);
    let val1 = parser.detector.face_locations(&matrix);
    if val1.len() < 1 { return FaceImage{ path: "none".to_string(), vector_puntos: Vec::new(), dirpath: "".to_string() }; }
    let val2 = parser.landmarks.face_landmarks(&matrix, &val1[0]);
    let val3 = parser.model.get_face_encodings(&matrix, &[val2], 0);
    let points = &val3[0];
    FaceImage{
      path: "".to_string(),
      dirpath: "".to_string(),
      vector_puntos: points.to_vec()
    }
  }


  pub fn to_box(&self) -> ImageBox {
    ImageBox::default(&self.vector_puntos)
  }
}
