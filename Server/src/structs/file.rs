use serde::{Serialize, Deserialize};
use hashbrown::HashMap;
use crate::structs::face::*;
use bson::*;
use std::fs;
use std::mem::swap;

pub struct FileHandler {
  parser: FaceParser,
  dirs: Vec<Dir>
}

impl FileHandler {
  pub fn new() -> Self {
    let data = fs::read(format!("{}/images/metadata.bson", env!("CARGO_MANIFEST_DIR"))).unwrap();
    let mut doc = bson::decode_document(&mut data.as_slice()).unwrap();
    let arrdocs = doc.get_array_mut("dirs").unwrap();
    let mut dirss = Vec::new();
    while let Some(top) = arrdocs.pop() {
      let dir: Dir = bson::from_bson(top).unwrap();
      dirss.push(dir);
    }
    FileHandler {
      parser: FaceParser::new(),
      dirs: dirss
    }
  }

  pub fn parsemem(&self, byteslice: &[u8]) -> Vec<f64> {
    self.parser.parsemem(byteslice)
  }

  pub fn loadimages(&mut self, names: &[&str]) {
    for name in names {
      let path = &format!("{}/images/{}", env!("CARGO_MANIFEST_DIR"), name);
      println!("Directory: {}", &name);
      let files = fs::read_dir(path).unwrap();
      let mut dir = Dir{files: Vec::new(), name: path.clone()};
      for i in files {
        let i = i.unwrap().path();
        let mut image = self.parser.parse(&i);
        if image.path != "none" {
          // image.dirpath = path.clone();
          image.dirpath = name.to_string();
          dir.files.push(image);
          if let Some(file) = i.file_name() {
            if let Some(file) = file.to_str() {
              println!("\tAdded: {}", file);
            }
          }
        }
      }
      self.dirs.push(dir);
    }
    self.savedirs();
  }

  fn savedirs(&mut self) {
    let dirs = bson::to_bson(&self.dirs).unwrap();
    let savedoc = doc!{ "dirs": dirs };
    let mut vectbits = Vec::new();
    bson::encode_document(&mut vectbits, &savedoc).unwrap();
    fs::write(format!("{}/images/metadata.bson", env!("CARGO_MANIFEST_DIR")), vectbits).unwrap();
  }

  pub fn get_file_map(&mut self) -> HashMap<String, FaceImage> {
    let mut map = HashMap::new();
    let mut filemap = Vec::new();
    swap(&mut self.dirs, &mut filemap);
    for dir in filemap {
      for file in dir.files {
        let path = format!("{:?}", file.vector_puntos);
        map.insert(path, file);
      }
    }
    map
  }
}

#[derive(Serialize, Deserialize)]
struct Dir {
  pub files: Vec<FaceImage>,
  pub name: String
}
