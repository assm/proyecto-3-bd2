use rstar::RTree;
use std::cmp::Reverse;
use std::cmp::Ordering;
use hashbrown::HashMap;
use crate::structs::point::*;
use priority_queue::PriorityQueue;
use crate::structs::face::FaceImage;
use crate::structs::file::FileHandler;
use hashbrown::hash_map::DefaultHashBuilder;

#[derive(Debug)]
struct Mytry {
  val: f64
}

impl Ord for Mytry {
  fn cmp(&self, other: &Self) -> Ordering {
    if self.val == other.val {
      Ordering::Equal
    } else if self.val > other.val {
      Ordering::Greater
    } else {
      Ordering::Less
    }
  }
}

impl PartialOrd for Mytry {
  fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
    Some(self.cmp(other))
  }
}

impl PartialEq for Mytry {
  fn eq(&self, other: &Self) -> bool {
    self.val == other.val
  }
}

impl Eq for Mytry {}

pub struct Index {
  filehandler: FileHandler,
  files: HashMap<String, FaceImage>,
  tree: RTree<ImageBox>,
}

impl Index {
  pub fn new() -> Self {
    let mut index = Index {
      files: HashMap::new(),
      filehandler: FileHandler::new(),
      tree: RTree::new(),
    };
    index.load();
    index
  }

  fn load(&mut self) {
    let mut items = Vec::new();
    self.files = self.filehandler.get_file_map();
    for (_, j) in &self.files {
      items.push(j.to_box());
    }
    self.tree = RTree::bulk_load(items);
  }

  pub fn parsemem(&self, byteslice: &[u8]) -> Vec<f64> {
    self.filehandler.parsemem(byteslice)
  }

  pub fn knn_seq_man(&mut self, k: usize, point: &[f64]) -> Vec<String> {
    let mut k = k;
    let mut queue = PriorityQueue::<_, _, DefaultHashBuilder>::with_default_hasher();
    for(_, j) in &self.files {
      queue.push(&j.path, Reverse(Mytry{val: Self::manhattan(&j.vector_puntos, point)}));
    }
    let mut result = Vec::new();
    while let Some((x, _)) = queue.pop() {
      if k == 0 {
        break;
      }
      result.push(x.to_string());
      k -= 1;
    }
    result
  }

  pub fn knn_seq_euc(&mut self, k: usize, point: &[f64]) -> Vec<String> {
    let mut queue = PriorityQueue::<_, _, DefaultHashBuilder>::with_default_hasher();
    let mut k = k;
    for (_, j) in &self.files {
      queue.push(&j.path, Reverse(Mytry{val: Self::euclediana(&j.vector_puntos, point)}));
    }
    let mut result = Vec::new();
    while let Some((x, _)) = queue.pop() {
      if k == 0 {
        break;
      }
      result.push(x.to_string());
      k -= 1;
    }
    result
  }

  pub fn knn_rtree(&mut self, k: usize, point: &[f64]) -> Vec<String> {
    let point = ImagePoint::default(point);
    let mut k = k;
    let mut result = Vec::new();
    for i in self.tree.nearest_neighbor_iter(&point) {
      if k == 0 {
        break;
      }
      result.push(self.files[&format!("{:?}", i.coordinates)].path.clone());
      k -= 1;
    }
    result
  }

  fn euclediana(x1: &[f64], x2: &[f64]) -> f64 {
    let mut acumulado = 0.0;
    let mut auxiliar;
    for x in 0..x1.len() {
      auxiliar = x1[x] - x2[x];
      auxiliar = auxiliar.powi(2);
      acumulado += auxiliar;
    }
    acumulado = acumulado.sqrt();
    acumulado
  }

  fn manhattan(x1: &[f64], x2: &[f64]) -> f64 {
    let mut acumulado = 0.0;
    let mut auxiliar;
    for x in 0..x1.len() {
      auxiliar = x1[x] - x2[x];
      auxiliar = auxiliar.abs();
      acumulado += auxiliar;
    }
    acumulado
  }
}
