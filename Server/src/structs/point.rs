use std::ops::{Index,IndexMut};
use rstar::*;

#[derive(Copy, Clone, PartialEq, Debug)]
pub struct ImagePoint {
  x: [f64; 32],
  y: [f64; 32],
  z: [f64; 32],
  c: [f64; 32],
}

impl rstar::Point for ImagePoint {
  type Scalar = f64;
  const DIMENSIONS: usize = 128;

  fn generate(generator: impl Fn(usize) -> Self::Scalar) -> Self {
    let mut v1 = [0.0; 32];
    let mut v2 = [0.0; 32];
    let mut v3 = [0.0; 32];
    let mut v4 = [0.0; 32];
    for i in 0..32 { v1[i] = generator(i); }
    for i in 0..32 { v2[i] = generator(i+32); }
    for i in 0..32 { v3[i] = generator(i+64); }
    for i in 0..32 { v4[i] = generator(i+96); }
    ImagePoint {
      x: v1,
      y: v2,
      z: v3,
      c: v4
    }
  }

  fn nth(&self, index: usize) -> Self::Scalar {
    match index {
      0 ..= 31 => self.x[index],
      32 ..= 63 => self.y[index - 32],
      64 ..= 95 => self.z[index - 64],
      96 ..= 127 => self.c[index - 96],
      _ => unreachable!(),
    }
  }

  fn nth_mut(&mut self, index: usize) -> &mut Self::Scalar {
    match index {
      0 ..= 31 => &mut self.x[index],
      32 ..= 63 => &mut self.y[index - 32],
      64 ..= 95 => &mut self.z[index - 64],
      96 ..= 127 => &mut self.c[index - 96],
      _ => unreachable!(),
    }
  }
}

impl Index<usize> for ImagePoint {
  type Output = f64;

  fn index(&self, index: usize) -> &Self::Output {
    match index {
      0 ..= 31 => &self.x[index],
      32 ..= 63 => &self.y[index - 32],
      64 ..= 95 => &self.z[index - 64],
      96 ..= 127 => &self.c[index - 96],
      _ => unreachable!(),
    }
  }
}

impl IndexMut<usize> for ImagePoint {
  fn index_mut(&mut self, index: usize) -> &mut Self::Output {
    match index {
      0 ..= 31 => &mut self.x[index],
      32 ..= 63 => &mut self.y[index - 32],
      64 ..= 95 => &mut self.z[index - 64],
      96 ..= 127 => &mut self.c[index - 96],
      _ => unreachable!(),
    }
  }
}

impl ImagePoint {
  pub fn new() -> Self {
    ImagePoint {
      x: [0.0; 32],
      y: [0.0; 32],
      z: [0.0; 32],
      c: [0.0; 32],
    }
  }

  fn nth_mut(&mut self, index: usize) -> &mut f64 {
    match index {
      0 ..= 31 => &mut self.x[index],
      32 ..= 63 => &mut self.y[index - 32],
      64 ..= 95 => &mut self.z[index - 64],
      96 ..= 127 => &mut self.c[index - 96],
      _ => unreachable!(),
    }
  }

  fn nth(&self, index: usize) -> f64 {
    match index {
      0 ..= 31 => self.x[index],
      32 ..= 63 => self.y[index - 32],
      64 ..= 95 => self.z[index - 64],
      96 ..= 127 => self.c[index - 96],
      _ => unreachable!(),
    }
  }

  pub fn default(items: &[f64]) -> Self {
    let mut point = Self::new();
    for i in 0..128 {
      point[i] = items[i];
    }
    point
  }
}

pub struct ImageBox {
  pub coordinates: Vec<f64>
}

impl RTreeObject for ImageBox {
  type Envelope = AABB<ImagePoint>;

  fn envelope(&self) -> Self::Envelope {
    let mut point = ImagePoint::new();
    for i in 0..128 {
      let it = point.nth_mut(i);
      *it = self.coordinates[i];
    }
    AABB::from_point(point)
  }
}

impl PointDistance for ImageBox {
  fn distance_2(&self, point: &ImagePoint) -> f64 {
    let mut acmulado: f64 = 0.0f64;
    let mut auxiliar;
    for i in 0..128 {
      auxiliar = self.coordinates[i] - point[i];
      auxiliar = auxiliar.powi(2);
      acmulado += auxiliar;
    }
    acmulado.sqrt()
  }

  fn contains_point(&self, point: &ImagePoint) -> bool {
    for i in 0..128 {
      if self.coordinates[i] != point.nth(i) {
        return false;
      }
    }
    true
  }

  fn distance_2_if_less_or_equal(&self, point: &ImagePoint, max_distance_2: f64) -> Option<f64> {
    let distance = self.distance_2(point);
    if distance<=max_distance_2 {
      Some(distance)
    } else {
      None
    }
  }
}

impl ImageBox {
  pub fn new() -> Self {
    ImageBox {
      coordinates: Vec::new()
    }
  }

  pub fn default(items: &[f64]) -> Self {
    let mut values = Vec::new();
    for i in 0..128 {
      values.push(items[i]);
    }
    ImageBox {
      coordinates: values,
    }
  }

  pub fn distance_2(&self, point: &ImagePoint) -> f64 {
    let mut acmulado: f64 = 0.0f64;
    let mut auxiliar;
    for i in 0..128 {
      auxiliar = self.coordinates[i] - point[i];
      auxiliar = auxiliar.powi(2);
      acmulado += auxiliar;
    }
    acmulado.sqrt()
  }
}

