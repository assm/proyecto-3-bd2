use rocket::State;
use std::sync::Mutex;
use serde::Serialize;
use serde::Deserialize;
use crate::structs::index::Index;
use rocket_contrib::json::{Json, JsonValue};
use std::time::SystemTime;

#[derive(Serialize, Deserialize)]
pub struct File {
  bytes: Vec<u8>,
  k: usize,
}

// 0 -> RTREE
// 1 -> EUCLIDEAN
// 2 -> MANHATAN

#[post("/upload/<method>", format = "json", data="<myfile>")]
pub fn upload(method: usize, myfile: Json<File>, map: State<Mutex<Index>>) -> JsonValue {
  let mut index = map.lock().unwrap();
  let file = &*myfile;
  // println!("{:?}", &file.bytes);
  let values = index.parsemem(&file.bytes);
  // let now = SystemTime::now();
  let result = match method {
    0 => {
      index.knn_rtree(file.k, &values)
    },
    1 => {
      index.knn_seq_euc(file.k, &values)
    },
    2 => {
      index.knn_seq_man(file.k, &values)
    },
    _ => unreachable!()
  };
  // println!("{}", now.elapsed().unwrap().as_millis());
  json!(result)
}
