#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

use std::sync::Mutex;
use rocket_cors as cors;
use rocket::http::Method;
use crate::cors::{AllowedHeaders, AllowedOrigins};

pub mod routes;
pub mod structs;

fn main() {
  let allowed_origins = AllowedOrigins::all();
  let options = cors::CorsOptions {
    allowed_origins,
    allowed_methods: vec![Method::Get, Method::Post, Method::Delete]
      .into_iter()
      .map(From::from)
      .collect(),
    allowed_headers: AllowedHeaders::all(),
    expose_headers: ["Content-Type", "X-Custom"]
      .iter()
      .map(ToString::to_string)
      .collect(),
      allow_credentials: true,
    ..Default::default()
  }.to_cors().expect("CORS");

  let index = structs::index::Index::new();
  // let mut handler = structs::file::FileHandler::new();
  // handler.loadimages(&[]);
  rocket::ignite().mount("/", routes![routes::upload]).attach(options).manage(Mutex::new(index)).launch();
}
