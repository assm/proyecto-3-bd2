# Proyecto-3-BD2

# Integrantes:
|Integrantes|Trabajo|
|-----------|-------|
|Dario Toribio López|100%|
|Fernando de los Heros|100%|
|Andre Segovia|100%|

# Requerimientos
- Rust
- Cargo
- Librería dlib
- Un http server para cargar la página

# Procedimiento para correr

- Escribimos ```cd Server```
- Compilamos con ```cargo build --release```
- Ejecutamos con ```./target/release/server```
- Cargar en un servidor la carpeta UI/dist/UI

# Definiciones Importantes:
BSON: Es un archivo binario que mapea un elemento llave a valor, que guarda los vectores característicos.

### `Point.rs :` 
Definimos ImagePoint como un punto que esta en un espacio de 128 dimensiones, esto nos sirve para ubicar el vector característico en el espacio y hallar distancias respecto a este.
Definimos un ImageBox como un punto en una región del RTree. Este forma parte del RTree, a diferencia del ImagePoint.
### `Face. rs :`
Definimos FaceParser como una interfaz que lee una imagen y extrae su vector característico. Mientras que FaceImage se encarga de encapsular todos los datos de una imagen.
### `File. rs :`
Definimos Dir como una encapsulación de un directorio que nos sirve para estructurar el bson.
Definimos FileHandler se encarga de contener a los directorios y al FaceParser. Este es escencial para administrar el bson, desde su creación hasta su modificación y sellar su persistencia.
### `Index. rs :`
Definimos MyTrie como un contenedor de flotante para permitirnos la operación de comparacion al momento de hacerla con los vectores característicos. 
Definimos Index como nuestro índice principal que contiene a nuestro FileHandler, a un HashMap que nos permite mapear un vector característico con su imagen respectiva y nuestro RTree.
El index se encarga de mapear la data en el RTree trayendo las imagenes desde el FileHandler, de calcular las distancias Manhattan y Euclediana, y calcula las operaciones KNN.

# Experimento 1
|K|ED|MD|
|-|--|--|
|4|6ms|6ms|
|8|6ms|6ms|
|16|6ms|5ms|

En niveles de precisión, en este experimento el que nos da mejores resultados es la distancia euclediana.

# Experimento 2
|N|KNN-RTree|KNN-Secuencial|
|-|--|--|
|100|1.2ms|0.5ms|
|200|1.4ms|0.58|
|400|1.6ms|0.6ms|
|800|2.5ms|0.6ms|
|1600|2.8ms|0.7ms|
|3200|3ms|2ms|
|6400|6ms|4ms|
|12800|7ms|6ms|

En este caso, Rust no nos devuelve los tiempos esperados ya que al tener una velocidad superior a python no le cuesta nada hacer las iteraciones para un n no tan grande. Por lo que si queremos visualizar realmente el potencial del RTree necesitariamos indexar muchas mas imagenes.
Por lo que creemos que a partir de cargar 300 mil imágenes es donde se puede notar la diferiencia de eficiencia al usar el  RTree.

# Backend
Para el backend hemos usado el framework rocket, debido a que nuestro índice esta construido con el lenguaje Rust, el cual se caracteriza por tener velocidades similares a C/C++.

# Frontend
Para este caso, hemos usado Angular typescript, y nos hemos apoyado en el proyecto anterior para hacer esta interfaz.
# Link del video 
https://drive.google.com/file/d/1k7U6Z_sbQsD-xCWm3eezul-IXr-dv-wS/view?usp=sharing